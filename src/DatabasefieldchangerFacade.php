<?php

namespace Broject\Databasefieldchanger;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Broject\Databasefieldchanger\Skeleton\SkeletonClass
 */
class DatabasefieldchangerFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'databasefieldchanger';
    }
}
