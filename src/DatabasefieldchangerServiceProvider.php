<?php

namespace Broject\Databasefieldchanger;

use Illuminate\Support\ServiceProvider;

class DatabasefieldchangerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('databasefieldchanger.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'databasefieldchanger');

        // Register the main class to use with the facade
        $this->app->singleton('databasefieldchanger', function () {
            return new Databasefieldchanger;
        });
    }
}
