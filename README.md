# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/broject/databasefieldchanger.svg?style=flat-square)](https://packagist.org/packages/broject/databasefieldchanger)
[![Build Status](https://img.shields.io/travis/broject/databasefieldchanger/master.svg?style=flat-square)](https://travis-ci.org/broject/databasefieldchanger)
[![Quality Score](https://img.shields.io/scrutinizer/g/broject/databasefieldchanger.svg?style=flat-square)](https://scrutinizer-ci.com/g/broject/databasefieldchanger)
[![Total Downloads](https://img.shields.io/packagist/dt/broject/databasefieldchanger.svg?style=flat-square)](https://packagist.org/packages/broject/databasefieldchanger)

This package serves you a variety of form inputs that saves its value instandly without pressing the save button.

## Installation

You can install the package via composer:

```bash
composer require broject/databasefieldchanger
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email philipp.brosig98@outlook.de instead of using the issue tracker.

## Credits

- [Philipp Brosig](https://github.com/broject)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.